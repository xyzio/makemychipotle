package com.xyzio.random.chipotle.order;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ShareActionProvider;
import android.widget.TextView;

public class MainActivity extends Activity {

	private ShareActionProvider mShareActionProvider;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        setContentView(R.layout.activity_main);
        final TextView mTextView = (TextView) findViewById(R.id.activity_main_textview_title);
        mTextView.setText("Use Preferences To Exclude or Include Menu Items...");
        
        onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        
        mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.menu_share).getActionProvider();
        //mShareActionProvider.setShareIntent(getDefaultShareIntent());
        
        return true;
    }
    
	private Intent getDefaultShareIntent() {

    	//case R.id.menu_save:
		  Intent shareIntent = new Intent(Intent.ACTION_SEND);
		  shareIntent.setType("text/plain");
		  final TextView mBodyText = (TextView) findViewById(R.id.activity_main_textview_body);
		  String order = mBodyText.getText().toString();
		  order = "I just ordered a " + order + "!";
		  shareIntent.putExtra(Intent.EXTRA_TEXT, order);
		  return(shareIntent);
		  //startActivity(Intent.createChooser(shareIntent, "Share ..."));
		  //return true;
		
		
	}

	@Override
	public void onResume() {
		super.onResume();
		
		//Read in all options
		
		
	}
	
	public void makeMyChipotle(View view) {
		final TextView mTextView = (TextView) findViewById(R.id.activity_main_textview_title);
		
		final TextView mBodyText = (TextView) findViewById(R.id.activity_main_textview_body);
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		ArrayList<String> entrees = new ArrayList<String>();
		{		
			if (sharedPref.getBoolean("pref_burrito", false) == false) {
				entrees.add("Burrito");
			}
			if (sharedPref.getBoolean("pref_tacos", false) == false) {
				entrees.add("Tacos");
			}
			if (sharedPref.getBoolean("pref_burrito_bowl", false) == false) {
				entrees.add("Burrito Bowl");
			}
			if (sharedPref.getBoolean("pref_salad", false) == false) {
				entrees.add("Salad");
			}
		}
		
		ArrayList<String> meats = new ArrayList<String>();
		{
			if (sharedPref.getBoolean("pref_chicken", false) == false) {
				meats.add("Chicken");
			}
			if (sharedPref.getBoolean("pref_barbacoa", false) == false) {
				meats.add("Barbacoa");
			}
			if (sharedPref.getBoolean("pref_steak", false) == false) {
				meats.add("Steak");
			}
			if (sharedPref.getBoolean("pref_carnitas", false) == false) {
				meats.add("Carnitas");
			}
			if (sharedPref.getBoolean("pref_vegetarian", false) == false) {
				meats.add("Vegetarian");
			}
		}
		
		ArrayList<String> salsas = new ArrayList<String>();
		{
			if (sharedPref.getBoolean("pref_mild_salsa", false) == false) {
				salsas.add("mild");
			}
			if (sharedPref.getBoolean("pref_medium_salsa", false) == false) {
				salsas.add("medium");
			}
			if (sharedPref.getBoolean("pref_medhot_salsa", false) == false) {
				salsas.add("medium hot");
			}
			if (sharedPref.getBoolean("pref_hot_salsa", false) == false) {
				salsas.add("hot");
			}
		}
		
		ArrayList<String> extras = new ArrayList<String>();
		{
			if (sharedPref.getBoolean("pref_chips_and guacamole", false) == false) {
				extras.add("Chips and Guacamole");
			}
			if (sharedPref.getBoolean("pref_guacamole", false) == false) {
				extras.add("Guacamole");
			}
			if (sharedPref.getBoolean("pref_chips", false) == false) {
				extras.add("Chips");
			}
		}
		
		ArrayList<String> drinks = new ArrayList<String>();
		{
			if (sharedPref.getBoolean("pref_margarita", false) == false) {
				drinks.add("Margarita");
			}
			if (sharedPref.getBoolean("pref_beer", false) == false) {
				drinks.add("Beer");
			}
			if (sharedPref.getBoolean("pref_bottle_drink", false) == false) {
				drinks.add("Bottled Drink");
			}
			if (sharedPref.getBoolean("pref_soda", false) == false) {
				drinks.add("Soda");
			}
			if (sharedPref.getBoolean("pref_water", false) == false) {
				drinks.add("Water");
			}
		}
		
		Random rand = new Random();
		
		String yourOrder = "Your Order is:";
		registerForContextMenu(mTextView);
		mTextView.setText(yourOrder);
		
		String order;
		
		order = meats.get(rand.nextInt(meats.size())).toLowerCase(Locale.US) + " " + entrees.get(rand.nextInt(entrees.size())).toLowerCase(Locale.US) + " with " + salsas.get(rand.nextInt(salsas.size())).toLowerCase(Locale.US) + " salsa and ";
		order +=  extras.get(rand.nextInt(extras.size())).toLowerCase(Locale.US) + " on the side. " + drinks.get(rand.nextInt(drinks.size())).toLowerCase(Locale.US) + " to drink.";
		registerForContextMenu(mBodyText);
		mBodyText.setText(order);
		
		mShareActionProvider.setShareIntent(getDefaultShareIntent());
		
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch(item.getItemId()) {
    	case R.id.menu_settings:
    		startActivity(new Intent(this, PreferencesActivity.class));
    		//SetBackgroundColor();
    		return true;
    		
      /*  case R.id.menu_share:
  		  Intent shareIntent = new Intent(Intent.ACTION_SEND);
  		  shareIntent.setType("text/plain");
  		  final TextView mBodyText = (TextView) findViewById(R.id.activity_main_textview_body);
  		  String order = mBodyText.getText().toString();
  		  order = "I just ordered " + order + "!";
  		  shareIntent.putExtra(Intent.EXTRA_TEXT, order);
  		  return true;*/
  		  //startActivity(Intent.createChooser(shareIntent, "Share ..."));
  		  //return true;
    	}
    	return false;
    }
    
}
